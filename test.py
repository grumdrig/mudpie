#!/usr/bin/python
import numpy, time, geology

"""A script used to test map generation bits."""

reload(geology)  # just good while using IDLE

R = 128
SHAPE = (R,R)
CLEVEL = 0.25
start = time.time()

#print 'voronoi',
#v = geology.generate_voronoi(SHAPE)
#v = geology.normalize(v)
#print time.time() - start

print 'pink',
a = geology.generate_pink_noise(SHAPE,32)
a = geology.normalize(a)
print time.time() - start

"""
print 'smooth8',
a1 = a
for i in range(10):
  a1 = geology.smooth(a1)
print time.time() - start

print 'slope_map',
a2 = geology.normalize( geology.slope_map(a))
a1 = geology.normalize( geology.slope_map(a1))
print time.time() - start
"""

print 'erode',

a = geology.thermally_erode(a, 0.02, 0.5, True)
                 

"""
I=1024
a1 = (geology.hydrode(a, I, 1.0/I, 0.5, 0.2))
a2 = a1 #(geology.hydrode(a, I, 0.32, 0.1, 0.2))

c = numpy.concatenate((a,a1,a2))
"""
print time.time() - start

c = a

#a = geology.normalize(a * 2.0 + v)

"""
print 'meadow',
b = geology.meadowize(a, CLEVEL)
print time.time() - start

print 'water',
for i in range(5):
  w = geology.hydraulically_erode(b, CLEVEL, 0.3)
  w = geology.normalize(w)
  b -= w * 0.05
  print time.time() - start

"""

print 'scale',
c = geology.scaleup(c)
print time.time() - start

print 'display',
geology.display(c, geology.land_sea_palette(CLEVEL))
print time.time() - start








def go():
  SHAPE = (128,128)  # required to be powers of 2 when wrapping
  #display(distort(normalize(generate_voronoi((512,512), (-1,1), 10)),32,8))
  a = normalize(generate_grid(SHAPE))
  a = normalize(generate_pink_noise(SHAPE))
  #for r in [32,16,8,4,2]:
  #  a = distort(a,r,r/8.0)
  c = a + 0
  for f in [16.0, 12.0, 8.0, 6.0]:
    b = a + 0
    for r in [32,16,8,4,2]:
      b = distort(b,r,r/f)
    c = numpy.concatenate((c,b))
  display(scaleup(c))
  return


  a = normalize(generate_voronoi(SHAPE, (-1,1), 10))
  #a = normalize(generate_pink_noise(SHAPE, 64)))
  #a = generate_perlin_noise(SHAPE, range(1,6))
  #b = generate_perlin_noise(SHAPE, range(1,6))
  display(scaleup(numpy.concatenate(
    [numpy.concatenate([distort(a,block,sd) for sd in (block/16.0,block/8.0,block/4.0,block/2.0,block)])
     for block in (4,16,64)], 1)))
  #display(scaleup(numpy.concatenate((a,b))))#, geographic_palette())

  #c = generate_fault_noise(SHAPE, 1000)
  #d = (a + b + c)

  # render(c, geographic_palette())

#go()

#tile it with itself
#d = tile(b)
#display(d)

#e = smooth(d)
#e = d+0
#thermally_erode(e, 1000)

#display(scaleup(numpy.concatenate((d,e))))

