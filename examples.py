#!/usr/bin/python
import numpy, time, sys
from geology import *

"""Display some examples"""


R = int(sys.argv[1]) if len(sys.argv) > 1 else 96
SHAPE = (R,R) # required to be powers of 2 when wrapping
CLEVEL = 0.25

print "This will take some time.",
sys.stdout.flush()

def tick():
  print ".",
  sys.stdout.flush()

g = Grid(R).normalize()

tick()

w = WhiteNoise(R).normalize()

tick()

k = MidpointDisplacementNoise(R, 32).normalize()

tick()

v = Voronoi(R, (-1,1), 10)
v.normalize()
v.distort(32,8)

tick()

p = PerlinNoise(R).normalize()

tick()

s = SpectralSynthesis(R, 0.9, 20).normalize()

tick()

f = FaultNoise(R, 100)
f.thermally_erode(0.02, 0.5, True)
f.normalize()

tick()

set = (g,w,k,v,p,s,f)
examples = image(numpy.concatenate(
  [numpy.concatenate((x.z,
                      x.copy().smooth().z,
                      x.copy().thermally_erode(0.02, 0.5, True).z,
                      Terrain(data=x.slope_map()).normalize().z,
                      numpy.square(x.copy().normalize().z),
                      x.copy().distort_fractally().z),
                     axis=1)
   for x in set]), land_sea_palette())

examples.save("examples.png")
examples.show()



"""
print 'slope_map',
a2 = geology.normalize( geology.slope_map(a))
a1 = geology.normalize( geology.slope_map(a1))
print time.time() - start
"""

"""
I=1024
a1 = (geology.hydrode(a, I, 1.0/I, 0.5, 0.2))
a2 = a1 #(geology.hydrode(a, I, 0.32, 0.1, 0.2))

c = numpy.concatenate((a,a1,a2))
"""


"""
print 'meadow',
b = geology.meadowize(a, CLEVEL)

print 'water',
for i in range(5):
  w = geology.hydraulically_erode(b, CLEVEL, 0.3)
  w = geology.normalize(w)
  b -= w * 0.05
"""


