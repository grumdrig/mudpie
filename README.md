mudpie: Python tools for procedural terrain generation 
------------------------------------------------------

Python routines for noise synthesis, erosion, etc. for creation of
realistic terrain heightmaps.

Project is in somewhat rough shape just now.

![examples.png](https://bitbucket.org/repo/a65d6e/images/430759872-examples.png)

*Columns: grid, white noise, pink noise, Voronoi, Perlin, spectral synthesis, fault noise*

*Rows: original, smoothed, after thermal erosion, slope map, squared, distorted*

CAVEATS

- Some non-essential methods require PIL (Python Imaging Library).

- flyby.py requires Soya3D, which isn't available on the Mac