#!/usr/bin/python
import numpy, time, sys
from geology import *

"""Generate the project logo"""

SHAPE = (192,192)

f = FaultNoise(SHAPE[0], 100)
f.thermally_erode(0.02, 0.5, True)
f.normalize()
f.distort_fractally()

logo = f.image(land_sea_palette(0.4))

logo = logo.crop((0,0,153,55))
logo.save("logo.png")
logo.show()
